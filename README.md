# Flutter Application - Product List

## Overview
This Flutter application allows users to browse and select products from a list. It follows the MVC (Model-View-Controller) pattern to structure the codebase. The application includes features such as updating product counters and displaying a cart with the total selected products.

## Features
- **Main Page**: The initial page of the application, serving as the entry point to the product list.
- **Product List**: Displays a list of products retrieved from the data model (`product_model.dart`). Users can update the product counter by clicking the "Buy Now" button.
- **Counter Limit**: When a product's counter reaches 5, a popup dialogue is triggered to inform the user. To prevent selecting more than 5 products, the "Buy Now" button is disabled using a null parameter with a ternary condition.
- **Floating Action Bar**: Contains a cart icon that navigates to the cart page and passes the updated product list as a parameter.
- **Cart Page**: Shows all the selected products from the product list page. The total number of products is calculated using the fold function and displayed in the body.
- **Back Button Behavior**: When the back button (appbar default back) is pressed, all product counters are reset to 0.

## Challenges and Solutions

### Challenge 1: Product Counter Limit

When a product's counter reaches 5, the showDialog function displays a popup. However, users could
continue clicking the counter and select more than 5 products.

**Solution**:
To address this issue, the "Buy Now" button is disabled when the counter reaches 5. This is achieved
by applying a ternary condition with a null parameter.

### Challenge 2: Back Button Behavior on Cart Page

After navigating to the Cart page, users can view the total number of selected products. However,
when pressing the default appbar back button, the counters for all products should be reset to 0.

**Solution**:
To achieve the desired behavior, the following steps were taken:

1. A reset function was implemented using a loop to set all product counters to 0.
2. The Scaffold on the Cart page was wrapped with the WillPopScope widget. The onWillPop callback
   was used to navigate back and pass a true value to indicate that the system back button was
   pressed.
3. The default appbar back button was replaced with a custom IconButton. The onPressed event
   triggers a Navigator.pop function with a true value.
4. The onPressed event also checks for a bool value, and if true, it triggers the Navigator.push
   function with the reset function as a condition.

## Usage

To use this application, follow these steps:

1. Clone the repository: `git clone [repository-url]`
2. Open the project in your preferred Flutter development environment.
3. Build and run the application on your device or emulator.

## Screenshots

<br />
<br />

<img src="https://gitlab.com/hasan082/product-list-ostad/-/raw/main/productlist1.png" alt="Column wrap List tile" width="240" >

<img src="https://gitlab.com/hasan082/product-list-ostad/-/raw/main/productlist2.png" alt="Grid view list view" width="240" >

<img src="https://gitlab.com/hasan082/product-list-ostad/-/raw/main/productlist3.png" alt="Grid view list view" width="240" >

## Dependencies

No dependency used

## License

This project is licensed under the [[MIT License](./LICENSE)] - see the [[MIT License](./LICENSE)]
file for details.

