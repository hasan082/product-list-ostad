import 'package:flutter/material.dart';
import 'package:product_list_update/models/product_model.dart';
import 'controller/product_controller.dart';

class CartPage extends StatelessWidget {
  final ProductListController productListController;

  const CartPage({super.key, required this.productListController});

  @override
  Widget build(BuildContext context) {
    int totalProducts = productListController.products
        .fold(0, (sum, product) => sum + product.counter);

    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, true);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            icon: const Icon(Icons.arrow_back),
          ),
          title: const Text('Cart'),
        ),
        body: SafeArea(
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Total Products:',
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                      width: 80,
                      height: 80,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                        color: Colors.blue
                      ),
                      child: Text(
                    '$totalProducts',
                    style: const TextStyle(fontSize: 25, color: Colors.white),
                  )),
                ]),
          ),
        ),
      ),
    );
  }
}
