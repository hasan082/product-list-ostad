import 'package:flutter/material.dart';
import 'carpage.dart';
import 'controller/product_controller.dart';

class ProductList extends StatefulWidget {
  const ProductList({super.key});

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final ProductListController productListController = ProductListController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product List'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
        child: ListView(
          children: [
            const SizedBox(
              height: 20,
            ),
            const Center(
              child: Text(
                "Welcome To Product List App",
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ListView.separated(
              shrinkWrap: true,
              physics: const ScrollPhysics(),
              itemCount: productListController.products.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              productListController.products[index].name,
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              '\$${productListController.products[index].price.toStringAsFixed(2)}',
                              style: const TextStyle(fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              productListController.products[index].counter
                                  .toString(),
                              style: const TextStyle(fontSize: 18),
                            ),
                            const SizedBox(
                              height: 3,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 13)),
                              onPressed: productListController
                                          .products[index].counter ==
                                      5
                                  ? null // Disable the button when counter reaches 5
                                  : () {
                                      setState(() {
                                        productListController
                                            .products[index].counter++;
                                        if (productListController
                                                .products[index].counter ==
                                            5) {
                                          _showCongratulationsDialog(
                                              productListController
                                                  .products[index].name);
                                        }
                                      });
                                    },
                              child: const Text(
                                'Buy Now',
                                style: TextStyle(fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 9.0),
                  child: Divider(
                    height: 19,
                    color: Colors.blue.shade400,
                  ),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 35, right: 25),
        child: FloatingActionButton(
          child: const Icon(Icons.shopping_cart),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CartPage(
                  productListController: productListController,
                ),
              ),
            ).then((value) {
              if (value == true) {
                _resetCounters();
              }
            });
            // _resetCounters();
          },
        ),
      ),
    );
  }

  void _resetCounters() {
    setState(() {
      for (var product in productListController.products) {
        product.counter = 0;
      }
    });
  }

  void _showCongratulationsDialog(String productName) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: const Text('Congratulations!'),
          content: Text('You\'ve bought 5 $productName!'),
          actions: [
            ElevatedButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
