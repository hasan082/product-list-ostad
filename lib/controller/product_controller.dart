import '../models/product_model.dart';

class ProductListController {

  List<Product> products = [
    Product(name: 'iPhone 12', price: 999.0),
    Product(name: 'Samsung Galaxy S21', price: 899.0),
    Product(name: 'Google Pixel 5', price: 699.0),
    Product(name: 'iPad Pro', price: 1099.0),
    Product(name: 'MacBook Pro', price: 1999.0),
    Product(name: 'AirPods Pro', price: 249.0),
    Product(name: 'Apple Watch Series 6', price: 399.0),
    Product(name: 'Sony PlayStation 5', price: 499.0),
    Product(name: 'Xbox Series X', price: 499.0),
    Product(name: 'Nintendo Switch', price: 299.0),
    Product(name: 'DJI Mavic Air 2', price: 799.0),
    Product(name: 'GoPro Hero9 Black', price: 449.0),
  ];

  void incrementCounter(int index) {
    products[index].counter++;
  }

  bool isCounterReachedLimit(int index) {
    return products[index].counter == 5;
  }

  void resetCounters() {
    for (var product in products) {
      product.counter = 0;
    }
  }

}
