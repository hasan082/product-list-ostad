class Product {
  String name;
  double price;
  int counter;

  Product({required this.name, required this.price, this.counter = 0});
}
